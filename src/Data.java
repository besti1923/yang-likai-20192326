abstract class Data {
    abstract public void DisplayValue();
}
class Integer extends  Data {
    int value;
    Integer() {
        value=88;
    }

    public static int parseInt(String arg) {
        return 0;
    }

    public void DisplayValue(){
        System.out.println (value);
    }
}
class Boolean extends Data{
    boolean value;
    Boolean(){
        value=true;
    }
    public void DisplayValue(){
        System.out.println(value);
    }
}
abstract class Factory {
    abstract public Data CreateDataObject();
}
class IntFactory extends Factory {
    public Data CreateDataObject(){
        return new Integer();
    }
}
class BooleanFactory extends Factory{
    public Data CreateDataObject(){
        return new Boolean();
    }
}
class Document {
    Data pd;
    Document(Factory pf){
        pd = pf.CreateDataObject();
    }
    public void DisplayData(){
        pd.DisplayValue();
    }
}
//Test class
class MyDoc {
    static Document d;
    static Document e;

    public static void main(String[] args) {
        d = new Document(new IntFactory());
        d.DisplayData();
        e = new Document(new BooleanFactory());                                                   //20175308
        e.DisplayData();

    }
}
