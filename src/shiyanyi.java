public class shiyanyi {
    private int element;
    private shiyanyi next;

    public shiyanyi(int element) {
        this.element = element;
        this.next = null;
    }

    public int getElement() {
        return element;
    }
    public void setElement(int element) {
        this.element = element;
    }
    public shiyanyi getNext() {
        return next;
    }
    public void setNext(shiyanyi next) {
        this.next = next;
    }

    @Override
    public String toString() {
        return ""+this.getElement();
    }
}