package sample;

import java.util.Arrays;
import java.util.Stack;

public class ArrayStack<T>{
    private final int DEFAULT_CAPACITY = 10;
    private int count;
    private T[] stack;
    public ArrayStack(){
        count = 0;
        stack = (T[]) (new Object[DEFAULT_CAPACITY]);
    }

    public T push(T element){
        if(count == stack.length){
            expandCapacity();
        }
        stack[count] = element;
        count += 1;
        return element;
    }

    @Override
    public String toString() {
        String rlt = "<TOP OF STACK>\n";
        for(int index=count-1; index >= 0;index --){
            rlt += stack[index] + "\n";
        }
        return rlt + "<BOTTOM OF STACK>";
    }

    public void expandCapacity(){
        stack = Arrays.copyOf(stack,stack.length + 100);
    }
    public boolean isEmpty(){
        if(count>0)return false;
        else return true;
    }
    public T pop() {
        if(!isEmpty()){
            count -= 1;
            return stack[count];
        }else return null;
    }
    public T peek(){
        if (!isEmpty()){
            return stack[count-1];
        }else return null;
    }
    public int size(){
        return count;
    }
}