package sample;

import java.util.StringTokenizer;

public class Complex{
    private int RealPart;
    private int ImagePart;
    private char ch;

    //构造函数
    public Complex(){}
    public Complex(String str){
        StringTokenizer st=new StringTokenizer(str,".",true);
        this.RealPart=Integer.parseInt(st.nextToken());
        this.ch=st.nextToken().charAt(0);
        this.ImagePart=Integer.parseInt(st.nextToken());
    }

    //返回
    public double getRealPart(){
        return RealPart;
    }

    public double getImagePart(){
        return ImagePart;
    }

    //接收
    public void setRealPart(int realPart){
        RealPart=realPart;
    }

    public void setImagePart(int imagePart){
        ImagePart=imagePart;
    }

    //重写函数toString
    public String toString(){
        return RealPart + "." + ImagePart;
    }

    public boolean equals(Object a){
        if (a==this){
            return true;
        }
        else{
            return false;
        }
    }

    // 定义公有方法:加减乘除
    public Complex ComplexAdd(Complex a){
        Complex b = new Complex((this.RealPart+a.RealPart)+"."+(this.ImagePart+a.ImagePart));
        return b;
    }
    public Complex ComplexSub(Complex a){
        Complex b = new Complex((this.RealPart-a.RealPart)+"."+(this.ImagePart-a.ImagePart));
        return b;
    }
    public Complex ComplexMulti(Complex a){
        Complex b = new Complex((this.RealPart*a.RealPart-this.ImagePart*a.ImagePart)+"."+(this.ImagePart*a.RealPart+this.RealPart*a.ImagePart));
        return b;
    }
    public Complex ComplexDiv(Complex a){
        double scale = a.getRealPart()*a.getRealPart() + a.getImagePart()*a.getImagePart();
        Complex b = new Complex((a.getRealPart() / scale)+"."+
                (- a.getImagePart() / scale));
        return b;
    }
}