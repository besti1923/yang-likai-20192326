package sample;

import java.util.Stack;

public class Stackdemo<T> extends ArrayStack<T> {
    public static void main(String[] args) {
        Stack stack = new Stack();
        stack.isEmpty();
        System.out.println("Static is empty:" +stack.empty());
        for(int i=0;i<=10;i++){
            stack.push(i);
            System.out.println("Static is:" + stack.toString()+"Static's size is:" + stack.size() + "");
        }
        stack.peek();
        for(int j=10;j>=0;j--){
            stack.pop();
            System.out.println("Static is:" + stack.toString()+"Static's size is:" + stack.size() + "");
        }
    }
}