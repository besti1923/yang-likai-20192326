package sample;

import junit.framework.TestCase;

public class Searching2Test extends TestCase {

    public void testLinearSearch()
    {



        double test[] = {1, 7, 11, 9, 23};//正常
        assertEquals(true, Searching2.linearSearch(test,  1));//边界
        assertEquals(true, Searching2.linearSearch(test,  23));//边界
        assertEquals(true, Searching2.linearSearch(test,  9));//正常
        assertEquals(false, Searching2.linearSearch(test,  100));//异常


        double test2[] = {1, 2, 3, 4, 5};//正序
        assertEquals(true, Searching2.linearSearch(test2,  1));//边界
        assertEquals(true, Searching2.linearSearch(test2,  5));//边界
        assertEquals(true, Searching2.linearSearch(test2,  3));//正常
        assertEquals(false, Searching2.linearSearch(test2,  100));//异常

        double test3[] = {5, 4, 3, 2, 1};//倒序
        assertEquals(true, Searching2.linearSearch(test3,  1));//边界
        assertEquals(true, Searching2.linearSearch(test3,  5));//边界
        assertEquals(true, Searching2.linearSearch(test3,  3));//正常
        assertEquals(false, Searching2.linearSearch(test3,  100));//异常

        double test4[] = {2, 0, 1, 8, 2, 3, 1, 2};//正常
        assertEquals(true, Searching2.linearSearch(test4,  2));//边界
        assertEquals(true, Searching2.linearSearch(test4, 2));//边界
        assertEquals(true, Searching2.linearSearch(test4,  8));//正常
        assertEquals(false, Searching2.linearSearch(test4,  100));//异常

        double ARR5[] = {1, 2, 3, 2};//正序
        assertEquals(true, Searching2.linearSearch(ARR5, 1));//边界
        assertEquals(true, Searching2.linearSearch(ARR5, 2));//边界
        assertEquals(true, Searching2.linearSearch(ARR5, 2));//正常
        assertEquals(false, Searching2.linearSearch(ARR5,  100));//异常

        double test6[] = {2, 3, 2, 1};//倒序
        assertEquals(true, Searching2.linearSearch(test6, 2));//边界
        assertEquals(true, Searching2.linearSearch(test6,  1));//边界
        assertEquals(true, Searching2.linearSearch(test6, 3));//正常
        assertEquals(false, Searching2.linearSearch(test6,  100));//异常


    }
    public void testBinarySearch() {
        Comparable test7[] = {1,2,3,4,5,6,7,2018,2312};

        assertEquals(4,Searching2.BinarySearch(test7,4));
        assertEquals(2312,Searching2.BinarySearch(test7,2312)); //边界
    }

    public void testInsertionSearch() {
        int test8[] = {1,2,3,7,2018,7,99,2312};
        assertEquals(true,Searching2.InsertSearch(test8,2,0,7));
        assertEquals(true,Searching2.InsertSearch(test8,2312,0,7)); //边界
    }


    public void testFibonacciSearch() {
        int test9[] = {1,2,3,4,7,8,9,15,2312};
        assertEquals(true,Searching2.FibonacciSearch(test9,8));
        assertEquals(true,Searching2.FibonacciSearch(test9,2312)); //边界

    }
}